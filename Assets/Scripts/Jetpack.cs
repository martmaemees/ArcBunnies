﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jetpack : MonoBehaviour {

    public float velocity = 20.0f;
    public Rigidbody2D jumper;

    private float timeLeft = 0.0f;

    private void Update()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0.0f) {
            timeLeft = 0.0f;
            gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        jumper.velocity = new Vector2(jumper.velocity.x, velocity);
    }

    public void AddTime(float time)
    {
        timeLeft += time;
        gameObject.SetActive(true);
    }

}
