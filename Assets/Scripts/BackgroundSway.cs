﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSway : MonoBehaviour {

    public Transform clouds;

    public float cloudsSpeed = 1.0f;

    private void Update()
    {
        clouds.position -= new Vector3((Mathf.Sin(Time.time / 3.0f)*0.2f + 1) * cloudsSpeed * Time.deltaTime, 0.0f, 0.0f);
        if (clouds.position.x <= -20.45f)
            clouds.position = new Vector3(0.0f, clouds.position.y, clouds.position.z);
        //backgroundHills.position = new Vector3(Mathf.Cos(Time.time / 2.0f) * backgroundHillsMultiplier, backgroundHills.position.y, backgroundHills.position.z);

        //ground.position = new Vector3(-Mathf.Sin(jumper.position.x * jumperMult), ground.position.y, ground.position.z);
    }

}
