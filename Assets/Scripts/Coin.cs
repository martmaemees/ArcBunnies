﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    public int scoreGain;

    private void FixedUpdate()
    {
        if (transform.position.y + 1.0f < Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, 0.0f)).y)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            FindObjectOfType<GameController>().AddScore(scoreGain);
            Destroy(gameObject);
        }
    }

}
