﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    public TextMeshProUGUI scoreText;

    private void Start()
    {
        if (PlayerPrefs.HasKey("HighScore"))
            scoreText.text = "High Score: " + PlayerPrefs.GetInt("HighScore").ToString();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Exit();
        }
    }

    public void Play()
    {
        SceneManager.LoadScene("Main");
    }

    public void Exit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    // Can't bind the button to BGM object, because it is not destroyed an load and get unbound when
    // Going to another scene.
    public void AudioToggle()
    {
        BGMController.bgmController.AudioToggle();
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

}
