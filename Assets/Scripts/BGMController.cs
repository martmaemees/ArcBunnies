﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BGMController : MonoBehaviour {

    public static BGMController bgmController;

    public AudioSource audioSource;
    public Image buttonImage;
    public Sprite audioOnSprite;
    public Sprite audioOffSprite;

    private void Awake()
    {
        if(bgmController != null)
        {
            Destroy(gameObject);
            return;
        }
        bgmController = this;
        DontDestroyOnLoad(gameObject);

        if(PlayerPrefs.HasKey("Audio"))
        {
            if (PlayerPrefs.GetInt("Audio") == 1)
            {
                Mute(false);
            }
            else
            {
                Mute(true);
            }
        }
        else
        {
            Mute(false);
        }
    }

    public void AudioToggle()
    {
        Mute(!audioSource.mute);
    }

    public void Mute(bool bMute)
    {
        audioSource.mute = bMute;
        if(buttonImage != null)
            buttonImage.sprite = bMute ? audioOffSprite : audioOnSprite;
        PlayerPrefs.SetInt("Audio", (bMute) ? 0 : 1);
    }

    public void OnLevelWasLoaded(int level)
    {
        if (GameObject.FindGameObjectWithTag("AudioButton"))
            buttonImage = GameObject.FindGameObjectWithTag("AudioButton").GetComponent<Image>();
        else
            buttonImage = null;
    }

}
