﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody2D))]
public class Jumper : MonoBehaviour {

    public float JumpVelocity = 10.0f;
    public float StrafeVelocity = 4.0f;
    public float StandDistance = 0.0f;
    public float SpringVelocity = 20.0f;

    public LayerMask PadLayer;

    public Sprite StandSprite;
    public Sprite JumpSprite;
    public SpriteRenderer SpriteRend;

    private Rigidbody2D rb;

    private GameObject pad;
    private float width;
    private float height;
    private float camHalfWidth;
    
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        width = JumpSprite.bounds.extents.x;
        height = JumpSprite.bounds.extents.y;
        camHalfWidth = Camera.main.orthographicSize * Camera.main.aspect;

        rb.velocity = new Vector2(0.0f, JumpVelocity);
	}
    
    void Update () {

	}

    private void FixedUpdate()
    {
        float xInput = 0.0f;
        xInput = Input.GetAxisRaw("Horizontal");
#if UNITY_ANDROID || UNITY_IOS
        foreach (Touch t in Input.touches)
        {
            if (!EventSystem.current.IsPointerOverGameObject(t.fingerId))
            {
                if (Camera.main.ScreenToWorldPoint(t.position).x >= 0)
                {
                    xInput = 1.0f;
                }
                else
                {
                    xInput = -1.0f;
                }
            }
        }
#endif


        rb.velocity = new Vector2(xInput * StrafeVelocity, rb.velocity.y);

        bool foundClose = false;
        if (rb.velocity.y <= 0.001f)
        {
            for (int i = -1; i < 2; i++)
            {
                if (Physics2D.Raycast(rb.position + new Vector2(i * width, -height), Vector2.down, StandDistance, PadLayer.value))
                {
                    foundClose = true;
                    break;
                }
            }

        
            for (int i = -1; i < 2; i++)
            {
                RaycastHit2D hit;
                if (hit = Physics2D.Raycast(rb.position + new Vector2(i * width, -height), Vector2.down, Mathf.Abs(rb.velocity.y) * Time.fixedDeltaTime, PadLayer.value))
                {
                    if(hit.collider.CompareTag("Spring"))
                    {
                        rb.position += new Vector2(0.0f, -hit.distance);
                        rb.velocity = new Vector2(rb.velocity.x, SpringVelocity);
                        hit.collider.GetComponent<Animator>().Play("Spring Animation");
                        break;
                    }
                    else
                    {
                        rb.position += new Vector2(0.0f, -hit.distance);
                        rb.velocity = new Vector2(rb.velocity.x, JumpVelocity);
                        hit.collider.GetComponent<Pad>().Jump();

                        break;
                    }
                }
            }
        }

        if (rb.position.x > camHalfWidth + 0.05f)
            rb.position = new Vector2(-camHalfWidth, rb.position.y);
        else if (rb.position.x < -camHalfWidth - 0.05f)
            rb.position = new Vector2(camHalfWidth, rb.position.y);

        if (foundClose)
            SpriteRend.sprite = StandSprite;
        else
            SpriteRend.sprite = JumpSprite;
    }

    private void OnDrawGizmosSelected()
    {
        if(rb)
        {
            Gizmos.color = Color.red;
            for (int i = -1; i < 2; i++)
            {
                Gizmos.DrawLine(rb.position + new Vector2(i * width, -height), rb.position + new Vector2(i * width, -height) + Vector2.down * StandDistance);
            }
        }
    }
}
