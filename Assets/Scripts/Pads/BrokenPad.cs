﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenPad : Pad {

    public override void Jump()
    {
        base.Jump();
        Destroy(gameObject);
    }

    public void FixedUpdate()
    {
        base.CheckToDestroy();
    }

}
