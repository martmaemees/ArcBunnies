﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pad : MonoBehaviour {

    private void FixedUpdate()
    {
        CheckToDestroy();
    }

    public virtual void Jump()
    {

    }

    protected void CheckToDestroy()
    {
        if (transform.position.y + 1.0f < Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, 0.0f)).y)
        {
            Destroy(gameObject);
        }
    }

}
