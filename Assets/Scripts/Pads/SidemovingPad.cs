﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SidemovingPad : Pad {

    public float speed = 1.0f;
    public float prefPathHalfWidth = 2.0f;

    public float maxX = 1.0f;
    public float minX = 0.0f;
    public float startX;
    public float startX2;

    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(speed, 0.0f);

        minX = rb.position.x - prefPathHalfWidth;
        maxX = rb.position.x + prefPathHalfWidth;

        startX = minX;
        startX2 = maxX;

        float camX = Camera.main.orthographicSize * Camera.main.aspect;
        float spriteX = GetComponentInChildren<SpriteRenderer>().sprite.bounds.extents.x;
        if(minX - spriteX < -camX)
        {
            minX = -camX + spriteX;
            maxX = minX + 2 * prefPathHalfWidth;
            if (maxX + spriteX > camX)
                maxX = camX - spriteX;

        } 
        else if(maxX + spriteX > camX)
        {
            maxX = camX - spriteX;
            minX = maxX - 2 * prefPathHalfWidth;
            if (minX - spriteX < -camX)
                minX = -camX + spriteX;
        }
    }

    private void FixedUpdate()
    {
        base.CheckToDestroy();
        if(rb.position.x > maxX)
        {
            rb.position = new Vector2(maxX, rb.position.y);
            rb.velocity = new Vector2(-speed, 0.0f);
        }
        else if(rb.position.x < minX)
        {
            rb.position = new Vector2(minX, rb.position.y);
            rb.velocity = new Vector2(speed, 0.0f);
        }
    }

}
