﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetpackItem : MonoBehaviour {

    public float timeAddition = 5.0f;

    private void FixedUpdate()
    {
        if (transform.position.y + 1.0f < Camera.main.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, 0.0f)).y)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            collision.gameObject.GetComponentInChildren<Jetpack>(true).AddTime(timeAddition);
            Destroy(gameObject);
        }
    }

}
