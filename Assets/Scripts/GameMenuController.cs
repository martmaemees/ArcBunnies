﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuController : MonoBehaviour {

    public GameController gameController;
    public GameObject pauseButton;
    public GameObject pausePanel;

    private bool paused;

    private void Start()
    {
#if UNITY_ANDROID
        pauseButton.SetActive(false);
#endif
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused)
                Pause();
            else
                Exit();
        }

#if UNITY_ANDROID
        if (paused && Input.touchCount > 0)
            Resume();
#endif
        if (paused && Input.GetMouseButtonDown(0))
            Resume();
    }

    public void Pause()
    {
        Time.timeScale = 0.0f;
#if !UNITY_ANDROID
        pauseButton.SetActive(false);
#endif
        pausePanel.SetActive(true);
        paused = true;
    }

    public void Resume()
    {
        Time.timeScale = 1.0f;
#if !UNITY_ANDROID
        pauseButton.SetActive(true);
#endif
        pausePanel.SetActive(false);
        paused = false;
    }

    public void Exit()
    {
        PlayerPrefs.SetInt("HighScore", gameController.GetScore());
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Main Menu");
    }

}
