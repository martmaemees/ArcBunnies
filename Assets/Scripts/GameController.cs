﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

[System.Serializable]
public struct ObjectData
{
    public GameObject ob;
    public float chance;
    public Vector3 offset;
}

public class GameController : MonoBehaviour {

    public GameMenuController gameMenuController;

    public Transform Jumper;
    public float CameraCatchSpeed = 6.0f;
    public float dampTime = 0.15f;
    [Range(0.1f, 0.9f)]
    public float RiseThreshold = 0.7f;
    public float ScoreMultiplier = 1.0f;


    public List<ObjectData> pads;
    private List<GameObject> padObjects;
    private List<float> padChances;

    public List<ObjectData> otherObjects;
    private List<float> otherChances;

    public GameObject bronzeCoin;
    public float bronzeSpawnChance;
    public GameObject silverCoin;
    public float silverSpawnChance;
    public GameObject goldCoin;
    public float goldSpawnChance;

    public List<int> difficultyHeights;
    public float diffAddition = 0.1f;

    public TextMeshProUGUI ScoreText;
    public GameObject GameOverText;
    public GameObject ContinueText;

    private Camera camera;
    private Jumper jumperScript;

    private float lastPadY = -5.0f;
    private float lastPadX = 0.0f;
    private float padWidth = 0.0f;
    private float highestJumpY;
    private Vector3 velocity = Vector3.zero;
    // From 0.3f to 0.95f.
    private float diffStatus = 0.3f;

    private float cameraXBounds = 0.0f;
    private float score = 0.0f;

    private float deathTime = 0.0f;

    private void Start()
    {
        camera = Camera.main;
        jumperScript = Jumper.GetComponent<Jumper>();
        cameraXBounds = camera.orthographicSize * camera.aspect;

        if(pads.Count == 0)
        {
            Debug.Log("No pads for world gen!");
            return;
        }
        padWidth = pads[0].ob.GetComponentInChildren<SpriteRenderer>().sprite.bounds.extents.x;
        highestJumpY = Mathf.Pow(jumperScript.JumpVelocity, 2) / 2.0f / Mathf.Abs(Physics2D.gravity.y);

        CalculateChances();

        GenWorld();
    }

    private void Update()
    {
        if(Jumper == null)
        {
            if (deathTime == 0.0f)
            {
                GameOverText.SetActive(true);
                deathTime = Time.time;
            }
            if (ContinueText.activeSelf == false && Time.time > deathTime + 1.0f)
                ContinueText.SetActive(true);
            if(Time.time > deathTime + 1.0f && (Input.anyKeyDown || Input.touchCount > 1))
            {
                gameMenuController.Exit();
            }
        }
        
    }

    private void FixedUpdate()
    {
        if (Jumper != null)
        {
            CheckIfPlayerBelowCamera();
            CameraRise();
        }
    }

    public void AddScore(float ammount)
    {
        score += ammount;
        ScoreText.text = ((int)score).ToString();
    }

    void CalculateChances()
    {
        // Pads.
        float totalChance = 0.0f;
        foreach (ObjectData p in pads)
        {
            totalChance += p.chance;
        }
        padObjects = new List<GameObject>();
        padChances = new List<float>();
        float tNormalChance = 0.0f;
        foreach (ObjectData p in pads)
        {
            padObjects.Add(p.ob);
            padChances.Add(tNormalChance + p.chance / totalChance);
            tNormalChance += p.chance / totalChance;
        }
        if (padChances[padChances.Count - 1] < 1.0f)
            padChances[padChances.Count - 1] = 1.0f;

        // Others.
        totalChance = 0.0f;
        foreach(ObjectData o in otherObjects)
        {
            totalChance += o.chance;
        }
        otherChances = new List<float>();
        tNormalChance = 0.0f;
        foreach(ObjectData o in otherObjects)
        {
            otherChances.Add(tNormalChance + o.chance / totalChance);
            tNormalChance += o.chance / totalChance;
        }
        if (otherChances[otherChances.Count - 1] < 1.0f)
            otherChances[otherChances.Count - 1] = 1.0f;
    }

    void CheckIfPlayerBelowCamera()
    {
        if (camera.WorldToScreenPoint(Jumper.position).y < 0.0f)
        {
            Destroy(Jumper.gameObject);
        }
    }

    void CameraRise()
    {
        float riseDistance = camera.WorldToScreenPoint(Jumper.position).y - camera.pixelHeight * RiseThreshold;
        if (riseDistance > 0.0f)
        {
            float yStart = camera.transform.position.y;
            float distanceToPosition = riseDistance / camera.orthographicSize / 2.0f;
            camera.transform.position = Vector3.SmoothDamp(camera.transform.position, camera.transform.position + Vector3.up * distanceToPosition, ref velocity, dampTime);
            AddScore((camera.transform.position.y - yStart) * ScoreMultiplier);
        }

        GenWorld();
    }

    void GenWorld()
    {

        float upperY = camera.ScreenToWorldPoint(new Vector3(0.0f, camera.pixelHeight * 3, 0.0f)).y;

        if(difficultyHeights.Count > 0 && upperY > difficultyHeights[0])
        {
            Debug.Log("HEIGHT: " + difficultyHeights[0]);
            difficultyHeights.RemoveAt(0);
            diffStatus += diffAddition;
            diffStatus = Mathf.Clamp(diffStatus, 0.3f, 0.95f);
        }

        while(lastPadY < upperY)
        {
            float nextY = Random.Range(lastPadY + (highestJumpY * diffStatus), lastPadY + (highestJumpY * Mathf.Clamp(diffStatus * 1.2f, 0.2f, 0.98f)));
            float nextX = Random.Range(-cameraXBounds + padWidth, cameraXBounds - padWidth);

            GameObject genPad = padObjects[0];
            float padValue = Random.Range(0.0f, 1.0f);
            for(int i = 0; i < padObjects.Count; i++)
            {
                // If randomized value is higher than current chance area, move on to next one.
                if (padValue < padChances[i])
                {
                    genPad = padObjects[i];
                    break;
                }
            }

            GameObject pad = (GameObject)Instantiate(genPad, new Vector3(nextX, nextY, 0.0f), Quaternion.identity);

            /** Gen any additional items before the next pad. */

            float r = Random.Range(0.0f, 1.0f);
            ObjectData ob = otherObjects[0];
            for(int i = 0; i < otherObjects.Count; i++)
            {
                if(r < otherChances[i])
                {
                    ob = otherObjects[i];
                    break;
                }
            }
            if(ob.ob != null)
            {
                Instantiate(ob.ob, pad.transform.position + ob.offset, Quaternion.identity, pad.transform);
            }

            // Add coins
            float coinValue = Random.Range(0.0f, 1.0f);
            if (coinValue <= goldSpawnChance)
                Instantiate(goldCoin, new Vector3(Random.Range(-cameraXBounds + 0.5f, cameraXBounds - 0.5f), Random.Range(nextY + 0.8f, (nextY + highestJumpY * 0.3f) - 0.8f), 0.0f), Quaternion.identity);
            //else if(coinValue <= silverSpawnChance)
            //    Instantiate(silverCoin, new Vector3(Random.Range(-cameraXBounds + 0.5f, cameraXBounds - 0.5f), Random.Range(nextY + 0.8f, (nextY + highestJumpY * 0.3f) - 0.8f), 0.0f), Quaternion.identity);
            else if(coinValue <= bronzeSpawnChance)
                Instantiate(bronzeCoin, new Vector3(Random.Range(-cameraXBounds + 0.5f, cameraXBounds - 0.5f), Random.Range(nextY + 0.8f, (nextY + highestJumpY * 0.3f) - 0.8f), 0.0f), Quaternion.identity);

         

            lastPadX = nextX;
            lastPadY = nextY;
        }
    }

    public int GetScore()
    {
        return (int)score;
    }

}
